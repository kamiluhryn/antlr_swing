tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer scopeNumber = 1;
}

prog    : (e+=zakres | e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d})
;

zakres : ^(BEGIN {scopeNumber = enterScope();} (e+=zakres | e+=expr | d+=decl)* {scopeNumber = leaveScope();}) -> block(wyr={$e}, dekl={$d}, i={scopeNumber+1})
;

decl  :
        ^(VAR i1=ID) {locals.newSymbol($ID.text + scopeNumber);} -> dek(n={$ID.text + scopeNumber})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> set(p1={$i1.text + getSymbol($ID.text, scopeNumber)}, p2={$e2.st})
        | ID                       -> id(n={$ID.text + getSymbol($ID.text, scopeNumber)})
        | INT                      -> int(i={$INT.text})
    ;
    